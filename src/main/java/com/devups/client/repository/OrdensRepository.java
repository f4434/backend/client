package com.devups.client.repository;

import com.devups.client.model.HistoryOrdenEntity;
import org.springframework.data.repository.CrudRepository;

public interface OrdensRepository extends CrudRepository<HistoryOrdenEntity, Long>{
    
}
