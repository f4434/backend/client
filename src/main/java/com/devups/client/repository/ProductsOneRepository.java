package com.devups.client.repository;

import java.util.List;

import com.devups.client.model.Productos;

import org.springframework.data.repository.CrudRepository;

public interface ProductsOneRepository extends CrudRepository<Productos, Long>{

    List<Productos> findByIdentificador(String string);
    
}
