package com.devups.client.repository;

import java.util.List;

import com.devups.client.model.entity.MesaOcupadaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MesaOcupadaRepository extends CrudRepository<MesaOcupadaEntity, Long>{
    List<MesaOcupadaEntity> findAllByNumeroMesa(String numeroMesa);
}
