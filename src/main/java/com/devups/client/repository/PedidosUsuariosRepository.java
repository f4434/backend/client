package com.devups.client.repository;

import java.util.List;

import com.devups.client.model.entity.PedidosUsuariosEntity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PedidosUsuariosRepository extends CrudRepository<PedidosUsuariosEntity, Long>{
    List<PedidosUsuariosEntity> findAllByNumeroMesaAndName(String numeroMesa, String name);
    List<PedidosUsuariosEntity> findAllByNumeroMesa(String numeroMesa);
    void deleteByNumeroMesa(String numeroMesa);
}
