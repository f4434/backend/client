package com.devups.client.repository;


import java.util.List;

import com.devups.client.model.Categoria;
import org.springframework.data.repository.CrudRepository;


public interface CategoryOneRepository extends CrudRepository<Categoria, Long>{

    List<Categoria> findByIdentificador(String string);
    
}
