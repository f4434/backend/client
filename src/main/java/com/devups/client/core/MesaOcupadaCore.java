package com.devups.client.core;

import java.util.List;

import com.devups.client.model.entity.MesaOcupadaEntity;


public interface MesaOcupadaCore {
    List<MesaOcupadaEntity> findAll();
    MesaOcupadaEntity create(MesaOcupadaEntity entity);
    MesaOcupadaEntity update(MesaOcupadaEntity entity);
    Boolean deleteAllMesa(Long id);
    List<MesaOcupadaEntity> findById(String id);
}
