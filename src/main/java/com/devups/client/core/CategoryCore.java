package com.devups.client.core;

import java.util.List;

import com.devups.client.model.Categoria;

public interface CategoryCore {
    List<Categoria> findAll();
    List<Categoria> create(List<Categoria> ordensEntity);
    Categoria update(Categoria ordensEntity);
    Boolean delete(Long id);
    List<Categoria> findAllById(Long id);
}
