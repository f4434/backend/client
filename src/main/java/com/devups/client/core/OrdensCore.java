package com.devups.client.core;

import java.util.List;
import java.util.Optional;

import com.devups.client.model.OrdensEntity;

public interface OrdensCore {
    List<OrdensEntity> findAll();
    OrdensEntity create(OrdensEntity ordensEntity);
    Optional<OrdensEntity> update(OrdensEntity ordensEntity);
    Boolean delete(Long id);
    OrdensEntity findById(Long id);
}
