package com.devups.client.core.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.devups.client.core.OrdensCore;
import com.devups.client.model.HistoryOrdenEntity;
import com.devups.client.model.Orden;
import com.devups.client.model.OrdensEntity;
import com.devups.client.repository.OrdenOneRepository;
import com.devups.client.repository.OrdensRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrdensCoreImpl implements OrdensCore {

    @Autowired
    OrdensRepository ordensRepository;

    @Autowired
    OrdenOneRepository ordenOneRepository;

    @Override
    public List<OrdensEntity> findAll() {
        List<HistoryOrdenEntity> ordensEntities = new ArrayList<>();
        ordensRepository.findAll().forEach(ordensEntities::add);
        List<Orden> orden = new ArrayList<>();
        ordenOneRepository.findAll().forEach(orden::add);

        List<OrdensEntity> dOrdensEntities = new ArrayList<>();
        for (HistoryOrdenEntity cccc : ordensEntities) {
            OrdensEntity dto = new OrdensEntity();
            var transform = toTransformEntity(cccc, dto, orden);
            dOrdensEntities.add(transform);
        }
        return dOrdensEntities;
    }

    public List<Orden> findAllOrden(String numeroOrden) {
        List<Orden> ordensEntities = new ArrayList<>();
        ordenOneRepository.findAll().forEach(ordensEntities::add);
        var listaFiltrada = ordensEntities.stream().filter(value -> value.getNumeroOrden().equals(numeroOrden)).collect(Collectors.toList());
        return listaFiltrada;
    }


    @Override
    public OrdensEntity create(OrdensEntity ordensEntity) {
        ordensEntity.setTotalAmount(calculeAmount(ordensEntity.getOrdens()));
        var ordenOnly = createOrden(ordensEntity.getOrdens());
        HistoryOrdenEntity historyOrdenEntity = new HistoryOrdenEntity();
        OrdensEntity ordensEntity2 = new OrdensEntity();
        var restul = toTransformHistory(historyOrdenEntity, ordensEntity);
        var bdCreade = ordensRepository.save(restul);
        return toTransformEntity(bdCreade, ordensEntity2, ordenOnly);
    }

    List<Orden> createOrden(List<Orden> ordenList){
        List<Orden> lista = new ArrayList<>();
        for (Orden orden : ordenList) {
            var rrr =ordenOneRepository.save(orden);
            lista.add(rrr);
        }
        return lista;
    }

    @Override
    public Optional<OrdensEntity> update(OrdensEntity ordensEntity) {
        ordensEntity.setTotalAmount(calculeAmount(ordensEntity.getOrdens()));
        /*return ordensRepository.findById(ordensEntity.getId())
        .map(old -> toTransform(old, ordensEntity))
        .map(ordensRepository::save);*/

        return null;
    }

    @Override
    public Boolean delete(Long id) {
        var ordenGeneral = ordensRepository.findById(id);
        findAllOrden(ordenGeneral.get().getNumeroOrden()).forEach(this::deleteOrdens);
        ordensRepository.deleteById(id);
        return true;
    }

    @Override
    public OrdensEntity findById(Long id) {
        OrdensEntity ordensEntity2 = new OrdensEntity();
        var xxxx = ordensRepository.findById(id);
        var yyyy = findAllOrden(xxxx.get().getNumeroOrden());
        return toTransformEntity(xxxx.get(), ordensEntity2, yyyy);  
    }

    OrdensEntity toTransform(OrdensEntity old, OrdensEntity nuw){
        old.setNameClient(nuw.getNameClient());
        old.setNameOperator(nuw.getNameOperator());
        old.setNumeroOrden(nuw.getNumeroOrden());
        old.setOrdens(nuw.getOrdens());
        old.setTotalAmount(nuw.getTotalAmount());
        return old;
    }

    Integer calculeAmount(List<Orden> ordenList){
        Integer amount = 0;
        for (Orden orden : ordenList) {
            amount += orden.getMonto();
        }
        return amount;
    }

    void deleteOrdens(Orden orden){
        ordenOneRepository.delete(orden);
    }


    HistoryOrdenEntity toTransformHistory(HistoryOrdenEntity old, OrdensEntity nuw){
        old.setNameClient(nuw.getNameClient());
        old.setNameOperator(nuw.getNameOperator());
        old.setNumeroOrden(nuw.getNumeroOrden());
        old.setTotalAmount(nuw.getTotalAmount());
        old.setNumeroMesa(nuw.getNumeroMesa());
        return old;
    }

    OrdensEntity toTransformEntity(HistoryOrdenEntity bd, OrdensEntity dto, List<Orden> ordens){
        dto.setNameClient(bd.getNameClient());
        dto.setNameOperator(bd.getNameOperator());
        dto.setNumeroOrden(bd.getNumeroOrden());
        dto.setTotalAmount(bd.getTotalAmount());
        dto.setNumeroMesa(bd.getNumeroMesa());
        dto.setId(bd.getId());
        dto.setOrdens(ordens);
        return dto;
    }

    
}
