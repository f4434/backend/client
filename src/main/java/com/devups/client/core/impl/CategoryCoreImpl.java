package com.devups.client.core.impl;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.devups.client.core.CategoryCore;
import com.devups.client.model.Categoria; 
import com.devups.client.repository.CategoryOneRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

@Service
public class CategoryCoreImpl implements CategoryCore {

    @Autowired
    CategoryOneRepository categoryOneRepository;

    @Override
    public List<Categoria> findAll() {
        return Streamable.of(categoryOneRepository.findAll()).toList();
    }

    @Override
    public List<Categoria> create(List<Categoria> entity) {
        return entity.stream().map(categoryOneRepository::save).collect(Collectors.toList());
    }

    @Override
    public Categoria update(Categoria entity) {
        return categoryOneRepository.save(entity);
    }

    @Override
    public Boolean delete(Long id) {
        categoryOneRepository.deleteById(id);
        return true;
    }

    @Override
    public List<Categoria> findAllById(Long id) {
        return categoryOneRepository.findByIdentificador("01");
    }







    
}
