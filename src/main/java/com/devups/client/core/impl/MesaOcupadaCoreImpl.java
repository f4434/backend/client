package com.devups.client.core.impl;

import java.util.List;
import com.devups.client.core.MesaOcupadaCore;
import com.devups.client.model.entity.MesaOcupadaEntity;
import com.devups.client.repository.MesaOcupadaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

@Service
public class MesaOcupadaCoreImpl implements MesaOcupadaCore {

    @Autowired
    MesaOcupadaRepository repository;

    @Override
    public List<MesaOcupadaEntity> findAll() {
        return Streamable.of(repository.findAll()).toList();
    }

    @Override
    public MesaOcupadaEntity create(MesaOcupadaEntity entity) {
        return repository.save(entity);
    }

    @Override
    public MesaOcupadaEntity update(MesaOcupadaEntity entity) {
        return repository.save(entity);
    }

    @Override
    public List<MesaOcupadaEntity> findById(String id) {
        return repository.findAllByNumeroMesa(id);
    }

    @Override
    public Boolean deleteAllMesa(Long id) {
        repository.deleteById(id);
        return true;
    }   
}
