package com.devups.client.controller;

import java.util.List;

import com.devups.client.core.CategoryCore;
import com.devups.client.model.Categoria;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/categoria")
public class CategoryController {
    
    @Autowired
    CategoryCore core;

    @GetMapping("/todo")
    List<Categoria> encuentraTodo(){
        return core.findAll();
    }
    
    @PostMapping("/crear")
    List<Categoria> crearOrden(@RequestBody List<Categoria> ordensEntity){
        return core.create(ordensEntity);

    }
    @PostMapping("/actualizar")
    Categoria actualizarOrden(@RequestBody Categoria ordensEntity){
        return core.update(ordensEntity);
    }
    @DeleteMapping("/{id}")
    Boolean borrarOrden(@PathVariable(value = "id") Long id){
        return core.delete(id);
    }

    @GetMapping("/by/{id}")
    List<Categoria> encontrarOrdenNumero(@PathVariable(value = "id") Long id){
        return core.findAllById(id);

    }
}
