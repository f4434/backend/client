package com.devups.client.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "MESA_OCUPADA")
public class MesaOcupadaEntity {
    
    @Id
    @SequenceGenerator(
            name = "sq_category",
            sequenceName = "sq_category",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "sq_category",
            strategy = GenerationType.SEQUENCE
    )
    private Long id;
    
    private String name;

    private String numeroMesa;

    private String empresa;
    
    @CreationTimestamp
    private Date fechaActual;
}
