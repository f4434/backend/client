package com.devups.client.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "PEDIDOS_USUARIOS")
public class PedidosUsuariosEntity {
    
    @Id
    @SequenceGenerator(
            name = "sq_pedidos",
            sequenceName = "sq_pedidos",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "sq_pedidos",
            strategy = GenerationType.SEQUENCE
    )
    private Long id;
    
    private String name;

    private String numeroMesa;

    private String empresa;

    private String platillo;

    private String comentario;
    
    private Integer precio;

    @CreationTimestamp
    private Date fechaPedido;
}
