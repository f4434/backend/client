package com.devups.client.model.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PedidosUsuarios {
    private Long id;
    private String name;
    private String empresa;
    private String numeroMesa;
    private List<SolicitudUsuario> solicitudUser;
    
}