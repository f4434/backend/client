package com.devups.client.model.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SolicitudUsuario {

    private String platillo;
    private String comentario;
    private Integer precio;
    private Date fechaPedido;
}
